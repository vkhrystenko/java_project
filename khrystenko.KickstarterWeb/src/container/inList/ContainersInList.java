package container.inList;

import java.util.ArrayList;
import java.util.List;




import container.Container;
import data.Data;

public abstract class ContainersInList<T extends Data> implements Container<T> {

	protected List<T> container = new ArrayList<T>();
//    protected Set<T> container = new LinkedHashSet<T>();
	
    @Override
	public T get(int index) {
		return container.get(index);
	}
	
    @Override
	public T getById(int id) {
		for(T item : container) {
			if(item.getId() == id) {
				return item;
			}
		}
		throw new IndexOutOfBoundsException();
	}
	

	@Override
	public void add(T content) {
		if(container.isEmpty()) {
			container.add(content);
		} else {
			for(T item: container) {
				if(item.equals(content)) { 
					return;
				}
			}
			container.add(content);
		}
	}
	
    @Override
	public int size() {
		return container.size();
	}
	
	public List<T> getContainer() {
		return container;
	}
}
