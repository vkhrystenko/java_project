package container;

import data.Quote;

public interface QuoteContainer  extends Container<Quote>{
	public abstract Quote getRandom();
}