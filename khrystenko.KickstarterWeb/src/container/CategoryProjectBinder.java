package container;

import data.Category;
import data.Project;

public abstract class CategoryProjectBinder {
	protected Container<Project> newcontainer;
	
	public abstract Container<Project> getListOfProjects(Category category, Container<Project> projectstorage);
}
