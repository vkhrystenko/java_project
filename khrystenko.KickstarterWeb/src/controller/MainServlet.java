package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import data.Category;
import data.Project;
import DAO.CategoriesDAO;
import DAO.ProjectsDAO;
import DAO.QuotesDAO;

public class MainServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String action = getAction(req);
		
		HttpSession session = req.getSession(false);
		
		CategoriesDAO categoriesDAO = new CategoriesDAO();
		List<Category> categories = categoriesDAO.getContainer();
		
		if (action.startsWith("/categories")) {
			
			QuotesDAO quotesDAO = new QuotesDAO();
			
			req.setAttribute("categories", categories);
			req.setAttribute("quote", quotesDAO.getRandom());
			
			req.getRequestDispatcher("categories.jsp").forward(req, resp);//redirest to JSP categories. JSP is also Servlet
			
		} else if (action.startsWith("/projects")) {
			ProjectsDAO projects = new ProjectsDAO();
			try {
				int categoryid = Integer.valueOf(req.getParameter("category"));
				
				List<Project> projects_with_category = projects.getContainer(categoriesDAO.getById(categoryid));
				
				session.setAttribute("categoryid", categoryid);
				session.setAttribute("projects", projects_with_category);
				req.getRequestDispatcher("projects.jsp").forward(req, resp);
			} catch (NumberFormatException | IndexOutOfBoundsException e) {
//				RequestDispatcher requestDispatcher = req.getRequestDispatcher("/categories");
//				req.setAttribute("error", "You entered wrong input! Try again.");
//				requestDispatcher.forward(req, resp);
				session.setAttribute("error", "You entered wrong input! Try again.");
				resp.sendRedirect("/khrystenko.KickstarterWeb/categories");
			}
		} else if (action.startsWith("/project")) {
//			ProjectsDAO projects = new ProjectsDAO();
			List<Project> projects = (List<Project>) session.getAttribute("projects");
			
			try {
				int projectid = Integer.valueOf(req.getParameter("projectid"));
	
				for( Project project: projects) {
					if(project.getId() == projectid) {
						session.setAttribute("project", project);
						
						req.getRequestDispatcher("project.jsp").forward(req, resp);
						return;
					}
				}
				throw new IndexOutOfBoundsException();
			} catch (NumberFormatException | IndexOutOfBoundsException e) {
				session.setAttribute("error", "You entered wrong input! Try again.");
				String redirectpage = "/khrystenko.KickstarterWeb/projects?category=" + session.getAttribute("categoryid");
				resp.sendRedirect(redirectpage);
			}
		} else if(action.startsWith("/donateproject")) {
			if (req.getParameterMap().containsKey("sum")) {
				try {
					double sum = Double.valueOf(req.getParameter("sum"));
					Project project_to_donate = (Project) session.getAttribute("project");
					project_to_donate.donate(sum);
					
					req.getRequestDispatcher("project_donation_done.jsp").forward(req, resp);
				} catch (NumberFormatException e) {
					session.setAttribute("error", "You entered wrong input! Try again.");
					String redirectpage = "/khrystenko.KickstarterWeb/donateproject";
					resp.sendRedirect(redirectpage);
				}
			} else {
				req.getRequestDispatcher("project_donation.jsp").forward(req, resp);
			}
		} 
	} 

	private String getAction(HttpServletRequest req) {
		String requestURI = req.getRequestURI();
		String action = requestURI.substring(req.getContextPath().length(), requestURI.length());
		return action;
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		//For studing purposes did not use POST method!
	}
	
	private void askUserForOption() {
		//TODO
	}
}
