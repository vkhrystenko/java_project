package data;

public class DonationLogic {
	private int totalAmount;
	private int daysLeft;
	private double collectAmount;
	private int id;
	private String donatorname;
	private int daonatorcardid;
	private static int donationid = 0;

	public DonationLogic(int totalAmount, int daysLeft) {
		this.totalAmount = totalAmount;
		this.daysLeft = daysLeft;
		this.collectAmount = 0.0;
		this.id = ++donationid;
	}
	
	public void donate(double amount) {
		if (amount <= 0) {
			throw new IllegalArgumentException();
		}
		collectAmount += amount;
	}
	
	public int getTotalAmount() {
		return totalAmount;
	}

	public double getCollectAmount() {
		return collectAmount;
	}

	public int getDaysLeft() {
		return daysLeft;
	}

	public int getId() {
		return id;
	}
	
	public void setDonatorName(String name) {
		donatorname = name;
	}
	
	public void setCardId(int id) {
		donationid = id;
	}
	
	public void setCollectedAmount(double amount) {
		collectAmount = amount;
	}
}