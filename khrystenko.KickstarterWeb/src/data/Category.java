package data;

public class Category implements Data {
	public static int categoryid = 0;
	
	private int id;
	private String name;

	public Category(String name) {
		if (name == null) {
			throw new IllegalArgumentException();
		}
		this.id = ++categoryid;
		this.name = name;
	}
	
	public Category(int id, String name) {
		this(name);
		this.id = id;
	}
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Category other = (Category) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equalsIgnoreCase(other.name))
			return false;
		return true;
	}


	@Override
	public int getId() {
		return id;
	}
	
	@Override
	public String getName() {
		return name;
	}

}
