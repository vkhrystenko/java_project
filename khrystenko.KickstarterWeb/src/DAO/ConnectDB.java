package DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ConnectDB implements AutoCloseable {
	
	private static final String SQL_URL = "jdbc:postgresql://192.168.136.129:5432/kickstarter";
	private static final String SQL_USER = "kickstarter";
	private static final String SQL_PASSWORD = "kickstarter";
	
	private static List<Connection> connections = new ArrayList<Connection>();
	private static Connection connection = null;

	static {
		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Can not load postgresql driver for java application", e);
		}
	}
	
	public Connection getConnection()
    {
        if (connection != null) {
        	return connection;
        }
        return getConnection(SQL_URL, SQL_USER, SQL_PASSWORD);
    }
	
	 private Connection getConnection(String SQL_URL, String SQL_USER, String SQL_PASSWORD) {
		 try {
			connection = DriverManager.getConnection(SQL_URL, SQL_USER, SQL_PASSWORD);
			connections.add(connection);
			return connection;
		} catch (SQLException e) {
			throw new RuntimeException("Can not connect to database", e);
		}
	 }
	
	 public <T> T get(ConnectionRunner<T> runable) {
		 try{
			 return runable.run(connection);
		 } catch(SQLException e) {
			 throw new RuntimeException("Problem occured while working with database", e);
		 }
		 finally {
			 //do nothing 
		 }
	 }
	 
	@Override
	public void close() {
		if(connection != null) {
			try {
				connection.close();
			} catch (SQLException e) {
				throw new RuntimeException("Can not close connection to database", e);
			}
		}
	}
}
