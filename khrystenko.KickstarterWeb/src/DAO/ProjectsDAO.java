package DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import data.Category;
import data.Data;
import data.Project;

public class ProjectsDAO extends ContainersDAO<Project>{
	
	@Override
	public Project get(int index) {
		// TODO Auto-generated method stub
		return null;
	}
	
	private Project getProject() {
		tableincolumns = "project";
		int projectid = (int) getColumn("id");
		String projectname = (String) getColumn("name");
		String projectdescription = (String) getColumn("description");
		String projectlink = (String) getColumn("link");
		String projectqa = (String) getColumn("qa");
		
		tableincolumns = "category";
		String categoryname = (String) getColumn("name");
		int categoryid = (int) getColumn("id");
		
		tableincolumns = "donation";
		int totalamount = (int) getColumn("total_amount");
		int daysleft = (int) getColumn("days_left");
		double collected_amount =  (double) getColumn("collect_amount");
		clearFields();
		
		Project project = new Project(projectid,projectname, projectdescription, 
										new Category(categoryid, categoryname), totalamount, daysleft, collected_amount, connection);
		project.addQuestionAnswer(projectqa);
		project.setLink(projectlink);
		return project;
	}
	
	@Override
	public Project getById(int id) {
		ResultSet resultdata = loadSpecificRows(SELECT_PROJECT, id);
		
		try {	
			ResultSetMetaData md = resultdata.getMetaData();
			int columns = md.getColumnCount();
        
			while (resultdata.next()) {
				for(int i=1; i<=columns; ++i){
					objectfields.put(md.getColumnName(i),resultdata.getObject(i));
				}
				return getProject();
			}
		} catch (SQLException e) {
			throw new RuntimeException("Error while geting metadata from database", e);
		}
		return null;
	}

	@Override
	public void add(Project content) {
		table = content.getClass().getSimpleName().toLowerCase();
		tableincolumns = table;
		
		id = content.getId();
		
		setColumn("id", id);
		setColumn("name", content.getName());
		setColumn("qa", content.getQuestionsAndAnswers());
		setColumn("link", content.getLink());
		setColumn("category_id", content.getCategory().getId());
		setColumn("description", content.getDescription());
		
		try {
			insert();
		} catch (SQLException e ) {
			throw new RuntimeException("Error was received from database while inserting project", e);
		}
		
		clearFields();
		table = "donation";
		tableincolumns = table;
		
		setColumn("id", content.donation.getId());
		setColumn("total_amount", content.donation.getTotalAmount());
		setColumn("days_left", content.donation.getDaysLeft());
		setColumn("collect_amount", content.donation.getCollectAmount());
		setColumn("project_id", content.getId());
		
		try {
			insert();
		} catch (SQLException e ) {
			throw new RuntimeException("Error was received from database while inserting project", e);
		}
		clearFields();
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<Project> getContainer(Data category) {
		ResultSet resultdata = loadSpecificRows(SELECT_QUERY_FULL_PROJECT_WITH_CATEGORY, category.getId());

		try {	
			ResultSetMetaData md = resultdata.getMetaData();
			int columns = md.getColumnCount();
        
			while (resultdata.next()) {
				for(int i=1; i<=columns; ++i){
					objectfields.put(md.getColumnName(i),resultdata.getObject(i));
				}
				receiveddata.add(getProject());
			}
			if (receiveddata.isEmpty()) {
				return receiveddata;
			}
			List<Project> newreceiveddata = new ArrayList<Project>(receiveddata);
			receiveddata.clear();
			
			return newreceiveddata;
		} catch (SQLException e) {
			throw new RuntimeException("Error while geting metadata from database", e);
		}
	}

	@Override
	public List<Project> getContainer() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void truncate() {
		try {
			PreparedStatement delete_query = connection.prepareStatement(String.format(TRUNCATE_QUERY_CASCADE, "projects"));
			delete_query.execute();
		} catch (SQLException e) {
			throw new RuntimeException("Error was received from database while truncating tables", e);
		}
	}
}
