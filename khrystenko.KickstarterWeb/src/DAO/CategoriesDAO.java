package DAO;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import data.Category;
import data.Data;

public class CategoriesDAO extends ContainersDAO<Category> {
	
	@Override
	public Category get(int index) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void add(Category content) {
		tableincolumns = content.getClass().getSimpleName().toLowerCase();
		table = tableincolumns.substring(0, tableincolumns.length() - 1) + "ie";
		id = content.getId();
		
		setColumn("id", id);
		setColumn("name", content.getName().toLowerCase());
		
		try {
			insert();
		} catch (SQLException e ) {
			throw new RuntimeException("Error was received from database while inserting category", e);
		}
		
		clearFields();
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<Category> getContainer() {
		
		if( isLoadedCategory == false) {
			loadAll(LIST_QUERY_CATEGORIES);


			for(HashMap<String, Object> categoryfields: objects) {
				Category category = new Category((int) categoryfields.get("category_id"), (String) categoryfields.get("category_name"));
				receiveddata.add(category);
			}
		}
		
		return receiveddata;
	}

	@Override
	public List<Category> getContainer(Data selectedchoice) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Category getById(int id) {
		for(Category cat: receiveddata) {
			if(cat.getId() == id) {
				return cat;
			}
		}
		throw new IndexOutOfBoundsException();
	}
	
	@Override
	public void truncate() {
		try {
			PreparedStatement delete_query = connection.prepareStatement(String.format(TRUNCATE_QUERY_CASCADE, "categories"));
			delete_query.execute();
		} catch (SQLException e) {
			throw new RuntimeException("Error was received from database while truncating tables", e);
		}
	}
}
