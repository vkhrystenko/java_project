package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import container.Container;
import data.Data;

public abstract class ContainersDAO <T extends Data> implements Container<T> {
	protected static ConnectDB connectdb = new ConnectDB();
	protected Connection connection = connectdb.getConnection();
	
	protected static String DELETE_QUERY   = "DELETE FROM \"%1$s\" WHERE %1$s_id=?";
	protected static String TRUNCATE_QUERY   = "TRUNCATE TABLE \"%1$s\"";
	protected static String TRUNCATE_QUERY_CASCADE   = "TRUNCATE TABLE \"%1$s\" CASCADE";
	protected static String INSERT_QUERY   = "INSERT INTO \"%1$s\" (%2$s) VALUES (%3$s)";
	protected static String INSERT_QUERY_IF_NOT_EXIST   = "INSERT INTO \"%1$s\" (%2$s) SELECT %3$s WHERE NOT EXISTS (SELECT * FROM \"%1$s\"  WHERE %4$s)";
	protected static String LIST_QUERY_CATEGORIES     = "SELECT * FROM categories";
	protected static String SELECT_QUERY_FULL_PROJECT_WITH_CATEGORY = "SELECT * FROM \"projects\" INNER JOIN donations ON project_id =  donation_project_id"
			       										+ " INNER JOIN categories ON project_category_id = category_id where category_id = ?";
	protected static String RANDOM_QUOTE_QUERY   = "SELECT quote_name FROM quotes ORDER BY random() LIMIT 1";
	protected static String SELECT_PROJECT = "SELECT * FROM \"projects\" INNER JOIN donations ON project_id =  donation_project_id"
				+ " INNER JOIN categories ON project_category_id = category_id where project_id = ?";
	
	protected String table = null;
	protected String tableincolumns = null;
	protected int id = 0;
	protected List<T> receiveddata = new ArrayList<T>();
	protected HashMap<String, Object> objectfields = new HashMap<String, Object>();
	protected List<HashMap<String, Object>> objects = new ArrayList<HashMap<String, Object>>();
	boolean isLoadedCategory = false;
	
	
	public final void setColumn(String name, Object value) {
		objectfields.put(MessageFormat.format("{0}_{1}",tableincolumns, name), value);
    }
	
	protected void insert() throws SQLException {
		connectdb.get(new ConnectionRunner<Void>() {
			
			@Override
			public Void run(Connection connection) throws SQLException {
				ArrayList<Object> values = new ArrayList<Object>();
		        StringBuilder columns = new StringBuilder();
		        StringBuilder placeholder = new StringBuilder();
		        boolean last = true;
		        
		        for(Map.Entry<String, Object> entry : objectfields.entrySet())  {           
		            if (!last) {
		                columns.append(", ");
		                columns.append(entry.getKey());
		                values.add(entry.getValue());
		            } else {
		                last = false;
		                columns.append(entry.getKey());
		                values.add(entry.getValue());
		            }
		        }
		        
		        for (String str: getPlaceholders(objectfields.size())) {
		            placeholder.append(str);
		        }

		        PreparedStatement insert_query = connection.prepareStatement(String.format(INSERT_QUERY_IF_NOT_EXIST, table + "s", 
		                                                            columns.toString(), 
		                                                            placeholder, 
		                                                            tableincolumns + "_id" + "=" + getColumn("id")));
		        
		        for (int i = 1; i <= values.size(); i++) {
		            insert_query.setObject(i, values.get(i-1));
		        }
		        insert_query.setQueryTimeout(30);
		        insert_query.execute();
		        
		        insert_query.close();
		        
		        return null;
				}
			});
		}
	
	public void loadAll(String command) {
		connectdb.get(new ConnectionRunner<Void>() {
			@Override
			public Void run(Connection connection) throws SQLException {
				if(isLoadedCategory == false) {
						Statement select_query = connection.createStatement();
						select_query.setQueryTimeout(30);
						ResultSet return_data = select_query.executeQuery(command);
						
						ResultSetMetaData md = return_data.getMetaData();
						int columns = md.getColumnCount();
			        
						while (return_data.next()) {
							for(int i=1; i<=columns; ++i){
								objectfields.put(md.getColumnName(i), return_data.getObject(i));
							}
							HashMap<String, Object> newobjectfields = new HashMap<String, Object>(objectfields);
							objects.add(newobjectfields);
							clearFields();
						}
						isLoadedCategory = true;
				}
				return null;
			}
		});
	}
	
	public ResultSet loadSpecificRows(String command, int id) {
		return connectdb.get(new ConnectionRunner<ResultSet>() {
			@Override
			public ResultSet run(Connection connection) throws SQLException {
				PreparedStatement select_query = null;
				select_query = connection.prepareStatement(String.format(command));
				select_query.setInt(1, id);
				select_query.setQueryTimeout(30);
				ResultSet return_data = select_query.executeQuery();
				return return_data;
			}
			});
	}
	
	private static Collection<String> getPlaceholders(int size) {
        List<String> template = new ArrayList<String>();
        boolean last = true;
        
        for (int i = 0; i < size; i++) {
            if(!last) {
            	template.add(", ");
            	template.add("?");
            } else {
                last = false;
                template.add("?");
            }
        }
   
        return template; 
    }
	
	public final int getId() {
        return this.id;
    }
	public final void clearFields() {
		objectfields.clear();
	}
	
	public final Object getColumn(String name) {
        Object value = objectfields.get(MessageFormat.format("{0}_{1}",tableincolumns, name));
        if (value == null) {
        	throw new RuntimeException("No value is found!!!");
        }
        return value;
    }
	
	abstract public void truncate();

}
