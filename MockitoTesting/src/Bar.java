import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.mockito.runners.MockitoJUnitRunner;


public class Bar {
	private Foo foo;
	
	public Bar(Foo foo) {
		this.foo = foo;
	}
	
	public String bar(String parameter) {
		return foo.foo(parameter);
	}
	
	@RunWith(MockitoJUnitRunner.class)
	public class MockitoTestRunner {
		@Mock
		private Foo foo;
		
		@InjectMocks
		private Bar bar = new Bar(null);
		
		@Test
		public void test() {
			bar.bar("qwe");
			verify(foo).foo("qwe");
		}
	}
}
