import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class MockitoTestSetup {
	private Foo foo;
	private Bar bar;
	
	@Before
	public void init() {
		foo = mock(Foo.class);
		bar = new Bar(foo);
	}
	
	@Test
	public void test() {
		bar.bar("qwe");
		verify(foo).foo("qwe");
	}
	
	@Test
	public void ignoreParameter() {
		Foo foo = mock(Foo.class);
		Bar bar = new Bar(foo);
		
		bar.bar("hfjhadsjh");
		
		verify(foo).foo("hfjhadsjh");
	}
	
	@Test
	public void stubParameter() {
		Foo foo = mock(Foo.class);
		Bar bar = new Bar(foo);
		
		when(foo.foo("qwe")).thenReturn("asd");
		
		assertEquals("asd", bar.bar("qwe"));
	}
	
	@Test
	public void IgnoreStirng() {
		Foo foo = mock(Foo.class);
		Bar bar = new Bar(foo);
		
		when(foo.foo(anyString())).thenReturn("asd");
		
		assertEquals("asd", bar.bar("qwe"));
		assertEquals("asd", bar.bar("qwertrewtw"));
	}
	
	@Test
	public void scenarioMatches() {
		Foo foo = mock(Foo.class);
		Bar bar = new Bar(foo);
		
		when(foo.foo(matches("..."))).thenReturn("asd");
		
		assertEquals("asd", bar.bar("qwe"));
		assertNull(bar.bar("qwertrewtw"));
	}
	
	@Test
	public void basicMatchers() {
		Foo foo = mock(Foo.class);
		Bar bar = new Bar(foo);
		
		when(foo.foo(endsWith("we"))).thenReturn("asd");
		when(foo.foo(contains("w"))).thenReturn("asd");
		when(foo.foo(startsWith("we"))).thenReturn("asd");
		
		assertEquals("asd", bar.bar("qwe"));
	}
}
