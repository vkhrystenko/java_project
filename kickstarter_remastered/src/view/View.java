package view;

import content.Content;

public interface View <T extends Content>{
	String getDescription(T content);
	void showDescription(T content);
	String getDetailedDescription(T content);
	void showDetailedDescription(T content);
}
