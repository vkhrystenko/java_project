package view;

import printer.Printer;
import content.Project;

public class ProjectView extends ViewFiller<Project> {
	
	public ProjectView(Printer printer) {
		this.printer = printer;
	}
	
	@Override
	public String getDescription(Project project) {
		StringBuilder projectdescription = new StringBuilder();
		
		projectdescription.append(project.getId());
		projectdescription.append(" - " + project.getName() + "\n");
		projectdescription.append("    Description = " + project.getDescription() + "\n");
		projectdescription.append("    TotalAmount/CollectAmount   " + project.getTotalAmount() + "/" 
						+ project.getCollectAmount() + "\n");
		projectdescription.append("    DaysLeft = " + project.getDaysLeft() + "\n");
		projectdescription.append("    Category : " + project.getCategory().getName() + "\n");
		
		return projectdescription.toString();
	}
	
	@Override
	public String getDetailedDescription(Project project) {
		StringBuilder projectdetaileddescription = new StringBuilder();
		
		projectdetaileddescription.append(getDescription(project));
		projectdetaileddescription.append("    Link : " + project.getLink() + "\n");
		projectdetaileddescription.append("    Question and answers : " + project.getQuestionsAndAnswers() + "\n");
		
		return projectdetaileddescription.toString();
	};
}
