package view;

import content.Content;
import printer.Printer;

public abstract class ViewFiller<T extends Content> implements View<T> {
	protected Printer printer;
	
	public String getDescription(T content) {
		return "Content has no description";
	}
	
	public void showDescription(T content) {
		printer.show(getDescription(content));
	}
	
	public String getDetailedDescription(T content) {
		return getDescription(content);
	}
	
	public void showDetailedDescription(T content) {
		printer.show(getDetailedDescription(content));
	}
	
}
