package view;

import printer.Printer;
import content.Category;

public class CategoryView extends ViewFiller<Category>{
	
	public CategoryView(Printer printer) {
		this.printer = printer;
	}
	
	@Override
	public String getDescription(Category category) {
		return category.getId() + " - " + category.getName();
	}

	@Override
	public String getDetailedDescription(Category category) {
		return getDescription(category);
	}
}
