package view;

import content.Quote;
import printer.Printer;

public class QuoteView extends ViewFiller<Quote> {
	
	public QuoteView(Printer printer) {
		this.printer = printer;
	}
	
	@Override
	public String getDescription(Quote quote) {
		return "Quote: " + quote.getQuote();
	}
}
