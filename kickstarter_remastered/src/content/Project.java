package content;


public class Project implements Content {
	private static int projectid = 0;
	
	private int id;
	private String name;
	private String description;
	private Category category;
	private int totalAmount;
	private int daysLeft;
	private int collectAmount;
	private String link;
	private String questionsAndAnswers;
	
	public Project(String name, String description, Category category, int totalAmount, int daysLeft) {
		if (name == null || description == null || category == null || totalAmount <= 0 || daysLeft <= 0) {
			throw new IllegalArgumentException();
		}
		this.category = category;
		this.id = ++projectid;
		this.name = name;
		this.description = description;
		this.category = category;
		this.totalAmount = totalAmount;
		this.daysLeft = daysLeft;
		this.collectAmount = 0;
		this.link = "";
		this.questionsAndAnswers = "";
	}
	
	public void setLink(String link) {
		if (link == null) {
			throw new IllegalArgumentException();
		}
		this.link = link;
	}
	
	public void addQuestionAnswer(String questionAndAnswers) {
		if (questionAndAnswers == null) {
			throw new IllegalArgumentException();
		}
		this.questionsAndAnswers = questionAndAnswers;
	}
	
	public void donate(int amount) {
		if (amount <= 0) {
			throw new IllegalArgumentException();
		}
		collectAmount += amount;
	}
	
	@Override
	public int getId() {
		return id;
	}

	public Category getCategory() {
		return category;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public int getTotalAmount() {
		return totalAmount;
	}

	public int getCollectAmount() {
		return collectAmount;
	}

	public int getDaysLeft() {
		return daysLeft;
	}

	public String getLink() {
		return link;
	}

	public String getQuestionsAndAnswers() {
		return questionsAndAnswers;
	}
}
