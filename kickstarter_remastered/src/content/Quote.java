package content;

public class Quote implements Content {
	private static int quoteid = 0;
	
	private int id;
	private String quote;

	public Quote(String quote) {
		if (quote == null) {
			throw new IllegalArgumentException();
		}
		this.id = quoteid++;
		this.quote = quote;
	}

	@Override
	public int getId() {
		return id;
	}
	
	public String getQuote() {
		return quote;
	}
}