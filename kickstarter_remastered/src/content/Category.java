package content;


public class Category implements Content {
	private static int categoryid = 0;
	
	private int id;
	private String name;

	public Category(String name) {
		if (name == null) {
			throw new IllegalArgumentException();
		}
		this.id = ++categoryid;
		this.name = name;
	}
	
	@Override
	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}
}
