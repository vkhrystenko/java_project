package scanner;

import java.io.IOException;
import java.util.Scanner;

public class ConsoleScanner implements Scannering {
	@Override
	public int inputValue() throws IOException {
		Scanner scanner = new Scanner(System.in);
		return scanner.nextInt();
	}
}

