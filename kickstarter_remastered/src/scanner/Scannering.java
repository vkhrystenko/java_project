package scanner;

import java.io.IOException;

public interface Scannering {
	int inputValue() throws IOException;
}
