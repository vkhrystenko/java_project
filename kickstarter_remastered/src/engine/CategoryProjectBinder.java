package engine;

import content.Category;
import content.Project;
import storage.ProjectStorage;

public class CategoryProjectBinder {
	private ProjectStorage newcontainer = new ProjectStorage();
	
	public ProjectStorage getListOfProjects(Category category, ProjectStorage projectstorage) {
		for(Project project : projectstorage ) {
			if(project.getCategory().equals(category)) {
				newcontainer.add(project);
			}
		}
		return newcontainer;
	}
}
