package engine;

import java.io.IOException;

import printer.Printer;
import scanner.Scannering;
import storage.ContainerCreator;
import content.Content;
import view.View;


public class KickstarterBackend <T extends Content>{
	private Printer printer;
	private Scannering scanner;
	private View<T> view;
	
	public KickstarterBackend(Printer printer, Scannering scanner, View<T> view) {
		this.printer = printer;
		this.scanner = scanner;
		this.view = view;
	}
	public void showQuote(T quote) {
		view.showDescription(quote);
	}
	
	public Content processItems(ContainerCreator<T> container) throws IOException {
		while(true) {
			getDescriptions(container);
			int id = chooseItem();
			if(id == CloseKickstarter.EXIT0.getId()) {
				return CloseKickstarter.EXIT0;
			}
			try{
				return getItemById(id, container);
				} catch (Exception ignore) {}
			printer.show("***********************");
			printer.show("You put incorrect id. try again");
		}
	}
	
	private void getDescriptions(ContainerCreator<T> container) {	
		for(T content : container) {
			view.showDescription(content);
		}
	}
	
	private int chooseItem() throws IOException {
		printer.showinline("Enter your choice or 0 to goup -- > ");
		return scanner.inputValue();
	}
	
	private Content getItemById(int id, ContainerCreator<T> container) {
		return container.getById(id);
	}
}
