package engine;

import content.Content;

public enum CloseKickstarter implements Content{
	EXIT0 (0),
	EXITSTRING ("exit");
	
	private int id;
	private String exit;
	
	private CloseKickstarter(int id) {
		this.id = id;
	}
	
	private CloseKickstarter(String string) {
		this.exit = string;
	}
	
	public int getId() {
		return this.id;
	}
	
	public String getExit() {
		return exit;
	}
}
