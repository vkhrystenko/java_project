package kickstarter;


import java.io.IOException;

import storage.ProjectStorage;
import content.Project;
import content.Category;
import storage.CategoryStorage;
import printer.ConsolePrinter;
import printer.Printer;
import scanner.ConsoleScanner;
import scanner.Scannering;
import storage.QuoteStorage;
import content.Quote;

public class Runner {
	public static void main(String[] args) throws IOException {
	Printer printer = new ConsolePrinter();
	Scannering scanner = new ConsoleScanner();
	
	QuoteStorage quotes = new QuoteStorage();
	quotes.add(new Quote("Shit happens"));
	quotes.add(new Quote("Dont worry, be happy"));
	quotes.add(new Quote("To be or not to be"));

	Category sport = new Category("Sport");
	Category art = new Category("Art");
	CategoryStorage categories = new CategoryStorage();
	categories.add(sport);
	categories.add(art);
	
	Project project1 = new Project("sport arena", "arena in Kiev", sport, 10000, 50);
	project1.donate(40);
	project1.setLink("www.project1.com");
	project1.addQuestionAnswer("why1");

	Project project2 = new Project("cybersystem", "cybersystem skynet", sport, 100000, 200);
	project2.donate(500);
	project2.setLink("www.project2.com");
	project2.addQuestionAnswer("why2");

	Project project3 = new Project("French art nano", "big badaboom", art, 1000000, 1000);
	project3.donate(7000);
	project3.setLink("www.project3.com");
	project3.addQuestionAnswer("why3");
	
	ProjectStorage projects = new ProjectStorage();
	projects.add(project1);
	projects.add(project2);
	projects.add(project3);
	
	Kickstarter kickstarter = new Kickstarter(printer, scanner, quotes, categories, projects);
	kickstarter.run();
	}
}
