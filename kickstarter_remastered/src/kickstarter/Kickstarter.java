package kickstarter;

import java.io.IOException;

import content.Category;
import content.Project;
import content.Quote;
import engine.CategoryProjectBinder;
import engine.CloseKickstarter;
import engine.KickstarterBackend;
import printer.Printer;
import scanner.Scannering;
import storage.CategoryStorage;
import storage.ProjectStorage;
import storage.QuoteStorage;
import view.CategoryView;
import view.ProjectView;
import view.QuoteView;
import view.ShowMenuCategories;
import content.Content;

public class Kickstarter {
	private ShowMenuCategories showcategories = new ShowMenuCategories();
	private Printer printer;
	private Scannering scanner;
	private QuoteStorage quotes;
	private CategoryStorage categories;
	private ProjectStorage projects;
	
	public Kickstarter(Printer printer, Scannering scanner, QuoteStorage quotes, 
						CategoryStorage categories, ProjectStorage projects) {
		this.printer = printer;
		this.showcategories.printer = printer;
		this.scanner = scanner;
		this.quotes = quotes;
		this.categories = categories;
		this.projects = projects;
	}

	public void run() throws IOException {
		showQuotes();
		launchCategories();
		showEndString();
	}

	private void showQuotes() {
		QuoteView view = new QuoteView(printer);
		KickstarterBackend<Quote> quoteBackend = new KickstarterBackend<Quote>(showcategories.printer, scanner, view);
		quoteBackend.showQuote(quotes.getRandom());
	}
	
	private void launchCategories() throws IOException {
		CategoryView view = new CategoryView(printer);
		while(true) {
			showcategories.printer.show("Here you have categories");
			Content content = new KickstarterBackend<Category>(showcategories.printer, scanner, view).processItems(categories);
			if(content == CloseKickstarter.EXIT0) {
				return;
			}
			Category cat = (Category) content;
			launchProjects(cat);
		}
	}
	
	private void launchProjects(Category cat) throws NumberFormatException, IOException {
		ProjectView view = new ProjectView(printer);
		CategoryProjectBinder binder = new CategoryProjectBinder();
		ProjectStorage selectedprojects = (ProjectStorage) binder.getListOfProjects(cat, projects);
		while(true) {
			Content content = new KickstarterBackend<Project>(printer, scanner, view).processItems(selectedprojects);
			if(content == CloseKickstarter.EXIT0) {
				return;
			}
			Project proj = (Project) content;
			showDetailedProject(view, proj);
		}
	}
		
	private void showDetailedProject(ProjectView view, Project project) throws NumberFormatException, IOException {
		view.showDetailedDescription(project);
		while(true) {
			printer.show("Enter 0 to go back --> ");
			int id = scanner.inputValue();
			if(id == CloseKickstarter.EXIT0.getId()) {
				return;
			}
		}
	}
	
	private void showEndString() {
		printer.show("Kickstarter ended...");
	}
}
