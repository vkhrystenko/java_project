package storage;

import java.util.Random;
import content.Quote;

public class QuoteStorage extends ContainerCreator<Quote>{
	
	public Quote getRandom() {
		int randomIndex = new Random().nextInt(size());
		return container.get(randomIndex);
	}
}
