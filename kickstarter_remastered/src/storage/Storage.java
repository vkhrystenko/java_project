package storage;

import content.Content;

public interface Storage <T extends Content> {
	T getById(int id);
	void add(T content);
	int size();
}
