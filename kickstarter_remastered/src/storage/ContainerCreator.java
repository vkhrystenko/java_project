package storage;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import content.Content;

public abstract class ContainerCreator<T extends Content> implements Storage<T>, Iterable<T>{
	protected List<T> container = new ArrayList<T>();
	
	public T getById(int id) {
		for(T item : container) {
			if(item.getId() == id) {
				return item;
			}
		}
		throw new IndexOutOfBoundsException();
	}
	
	public void add(T content) {
		container.add(content);
	}
	
	public int size() {
		return container.size();
	}
	
	public Iterator<T> iterator() {        
        Iterator<T> result = container.iterator();
        
        return result; 
    }
}
