package kickstarter.data;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import container.CategoryProjectBinderFileandList;
import container.Container;
import container.inList.ProjectContainerInList;
import data.Category;
import data.Project;

public class ProjectsTest {
	Container<Project> container;
	CategoryProjectBinderFileandList binder;
	Container<Project> containerfound;
	
	@Before
	public void setup() {
		container = new ProjectContainerInList();
		binder = new CategoryProjectBinderFileandList();
		containerfound = new ProjectContainerInList();
		Project.projectid = 0;
	}
	
	@Test
	public void shouldEmptyProjects_whenEmptyProjects() {
		
		containerfound = binder.getListOfProjects(new Category("name1"), container);
		
		assertEquals(0, containerfound.size());
	}
	
	@Test
	public void shouldEmptyProjects_whenDifferentCategory() {

		Category category = new Category("category");
		Category art = new Category("art");
		
		Project project1 = new Project("sport arena", "arena in Kiev", art, 10000, 50);
		project1.setCategory(category);

		Project project2 = new Project("cybersystem", "cybersystem skynet", art, 100000, 200);
		project2.setCategory(category);

		Project project3 = new Project("French art nano", "big badaboom", art, 1000000, 1000);
		project3.setCategory(category);
		
		container.add(project1);
		container.add(project2);
		container.add(project3);
		
		containerfound = binder.getListOfProjects(art, container);
		
		assertEquals(0, containerfound.size());
	}
	
	@Test
	public void shouldProjects_whenCategoryFound() {

		Category category = new Category("category");
		Category art = new Category("art");
		
		Project project1 = new Project("sport arena", "arena in Kiev", art, 10000, 50);

		Project project2 = new Project("cybersystem", "cybersystem skynet", art, 100000, 200);

		Project project3 = new Project("French art nano", "big badaboom", art, 1000000, 1000);
		project3.setCategory(category);
		
		container.add(project1);
		container.add(project2);
		container.add(project3);
		
		containerfound = binder.getListOfProjects(art, container);
		
		assertEquals(2, containerfound.size());
		assertSame(project2, containerfound.get(1));
		assertSame(project1, containerfound.get(0));
	}
	
	@Test
	public void ExceptionThrowedConstructor() {
		Category category = new Category("category");
		boolean exception1Thrown = false;
	    try {
	        new Project(null, "test", category, 1, 1);
	    }catch(Exception e){
	        exception1Thrown = true;
	    }
	    assertTrue(exception1Thrown);

	    boolean exception2Thrown = false;
	    try {
	    	new Project("test", null, category, 1, 1);
	    }catch(Exception e){
	        exception2Thrown = true;
	    }
	    assertTrue(exception2Thrown);
	    
	    boolean exception3Thrown = false;
	    try {
	    	new Project("test", "test", category, -1, 1);
	    }catch(Exception e){
	        exception3Thrown = true;
	    }
	    assertTrue(exception3Thrown);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testDonating() {
		Project project = new Project("test", "test", new Category("category"), 100, 1);
		project.donate(-10);
	}
}