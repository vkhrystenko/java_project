package kickstarter.data;

import static org.junit.Assert.*;

import java.util.*;

import org.junit.Test;

import container.inList.QuoteContainerInList;
import data.Quote;

public class QuotesTest {
	
	class FakeRandom extends Random {
		private List<Integer> numbers;
		
		public FakeRandom(Integer... numbers) {
			this.numbers = new LinkedList<Integer>(Arrays.asList(numbers));
		}
		
		@Override
		public int nextInt(int i) {
			return numbers.remove(0);
		}
	}
	
	@Test
	public void shouldGenerateNewQuote() {
		//given
		QuoteContainerInList quotes = new QuoteContainerInList(new FakeRandom(0,1));
		quotes.add(new Quote("Shit happens"));
		quotes.add(new Quote("Dont worry, be happy"));
		
		//when
		Quote quote = quotes.getRandom();
		
		//then
		assertEquals("Shit happens", quote.getName());
		
		//when
		Quote quote2 = quotes.getRandom();
				
		//then
		assertEquals("Dont worry, be happy", quote2.getName()); 
	}
}
