package kickstarter.data;

import container.Container;
import container.inList.CategoryInList;
import data.Category;

public class CategoriesInListTest extends CategoryContainerTest {

	@Override
	Container<Category> getCategories() {
		return new CategoryInList();
	}

}
