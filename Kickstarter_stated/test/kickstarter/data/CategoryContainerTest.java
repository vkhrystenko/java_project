package kickstarter.data;

import static org.junit.Assert.*;

import org.junit.Before;
//import org.junit.Ignore;
import org.junit.Test;

import container.Container;
import data.Category;

public abstract class CategoryContainerTest {
	
	private Container<Category> categories;

	@Before
	public void setup() {
		categories = getCategories();
		Category.categoryid = 0;
	}

	abstract Container<Category> getCategories();
	
	@Test
	public void shouldBeCategories_whenAdded() {
		
		categories.add(new Category("name1"));
		categories.add(new Category("name2"));
		
		StringBuilder result = new StringBuilder();
		for(Category cat : categories.getContainer()) {
			result.append(cat.getName());
		}
		
		assertEquals("name1name2", result.toString());
	}
	
	//Try to test exceptions
	@Test
	public void shouldBeUniqueCategorywhenAddedTheSameCategory() {
		categories.add(new Category("name1"));
		categories.add(new Category("Name1"));
		categories.add(new Category("name2"));
		categories.add(new Category("name2"));
		
		StringBuilder result = new StringBuilder();
		for(Category cat : categories.getContainer()) {
			result.append(cat.getName());
		}
		
		assertEquals("name1name2", result.toString());
	}
	
	//TODO Check when you add many categories and static id
	
	@Test
	public void shouldBeCategories_noAdded() {
		
		StringBuilder result = new StringBuilder();
		for(Category cat : categories.getContainer()) {
			result.append(cat.getName());
		}
		
		assertEquals("", result.toString());
	}
	
	@Test
	public void shouldGetCategoryById() {
		
		Category category1 = new Category("name1");
		categories.add(category1);
		
		Category category2 = new Category("name2");
		categories.add(category2);
		
		Category.categoryid = 0;
		
		assertEquals(categories.size(), 2);
		assertEquals(category1, categories.getById(1));
		assertEquals(category2, categories.getById(2));
	}
	
	@Test
	public void shouldGetCategoryByIndex() {
		
		Category category1 = new Category("name1");
		categories.add(category1);
		
		Category category2 = new Category("name2");
		categories.add(category2);
		
		Category.categoryid = 0;
		
		assertEquals(categories.size(), 2);
		assertEquals(category1, categories.get(0));
		assertEquals(category2, categories.get(1));
	}
	
	@Test
	public void shouldGetSize() {

		assertEquals(0,categories.size());
		
		Category category1 = new Category("name1");
		categories.add(category1);
		
		Category category2 = new Category("name2");
		categories.add(category2);
		
		assertEquals(2, categories.size());
	}
}