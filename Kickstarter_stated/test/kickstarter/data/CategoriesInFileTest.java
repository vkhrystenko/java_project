 package kickstarter.data;

import java.io.File;

import org.junit.After;

import container.Container;
import container.inFile.CategoriesInFile;
import data.Category;

public class CategoriesInFileTest extends CategoryContainerTest {

	@Override
	Container<Category> getCategories() {
		return new CategoriesInFile("test-categories.txt");
	}
	
	@After
	public void cleanUp() {
		new File("test-categories.txt").delete();
	}
}
