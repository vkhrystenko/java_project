package kickstarter.kickstarter;

import org.junit.After;

import DAO.CategoriesDAO;
import DAO.ProjectsDAO;
import DAO.QuotesDAO;

public class KickstarterDAOTest extends KickstarterTest {
	
	@After
	public void clearup() {
		categories.truncate();
		quotes.truncate();
		projects.truncate();
	}
	
	@Override
	CategoriesDAO getCategories() {
		return new CategoriesDAO();
	}

	@Override
	ProjectsDAO getProjects() {
		return new ProjectsDAO();
	}

	@Override
	QuotesDAO getQuotes() {
		return new QuotesDAO();
	}
}
