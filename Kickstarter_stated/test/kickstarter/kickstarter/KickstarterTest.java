package kickstarter.kickstarter;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import kickstarter.Kickstarter;
import kickstarter.Storage;

import org.junit.Before;
//import org.junit.Ignore;
import org.junit.Test;

import views.ConsoleView;
import views.View;
import static org.mockito.Mockito.*;
import container.Container;
import container.QuoteContainer;
import data.Category;
import data.Project;
import data.Quote;
import IO.CheckStringDecorator;
import IO.IO;

public abstract class KickstarterTest {
	
	protected Storage storage;
	protected Container<Project> projects;
	protected Container<Category> categories;
	protected QuoteContainer quotes;

	@Before 
	public void setUp() {
		quotes = getQuotes();
		categories = getCategories();
		projects = getProjects();
		
		storage = new Storage(quotes, categories, projects);
		
		
		Project.projectid = 0;
		Category.categoryid = 0;
	}
	
	abstract QuoteContainer getQuotes();

	abstract Container<Project> getProjects();

	abstract Container<Category> getCategories();

	@Test
	public void dummy_stub() throws IOException {
		IO io = new IO() {

			@Override
			public void show(String string) {

			}

			@Override
			public void showinline(String string) {

			}

			@Override
			public String readline() throws IOException {
				return "0";
			}
			
		};		
		View view = new ConsoleView(io);
		Kickstarter kickstarter = new Kickstarter(io, storage, view);
		kickstarter.run();
	}
	
	class FakeIO implements IO {
		private List<String> messages = new LinkedList<String>();
		private List<String> input = new LinkedList<String>();
		
		public FakeIO(String... input) {
			this.input = new LinkedList<String>(Arrays.asList(input));//because Arrays.asList returns non-modified List!
		}

		@Override
		public void show(String string) {
			messages.add(string);
		}

		@Override
		public void showinline(String string) {
			this.show(string + "\n");
		}

		@Override
		public String readline() throws IOException {
			return input.remove(0);//take the first number and delete it
		}
		
		public List<String> getMessages() {
			return messages;
		}
	}
	
	class CheckStringDecoratorTest extends CheckStringDecorator {
		FakeIO io;
		
		public CheckStringDecoratorTest(FakeIO io) {
			super(io);
			this.io = io;
		}
		
		public List<String> getMessages() {
			return io.getMessages();
		}
	}
	
	@Test
	public void fake_no_projects_for_categories() throws IOException {
		//given
		FakeIO io = new FakeIO("1","0","0");//choose category, go back, go back and exit the program
		View view = new ConsoleView(io);
		
		categories.add(new Category("name1"));//the first test for fake is done with empty containers!!!
		categories.add(new Category("name2"));
		quotes.add(new Quote("testing quote"));
		
		Kickstarter kickstarter = new Kickstarter(io, storage, view);
		
		//when
		kickstarter.run();
		
		//then
		assertEquals("[Quote: testing quote, ----------Categories----------\n" + 
						"1 - name1\n2 - name2\n---------------------------------, \n" +
						"Please select your variant (0 to goback)--> \n" +
						", No projects exist for chosen category.\n" +  
						", \nPlease select your variant (0 to goback)--> \n" +
						", Quote: testing quote, ----------Categories----------\n" +
						"1 - name1\n2 - name2\n" + 
						"---------------------------------, \n" +
						"Please select your variant (0 to goback)--> \n" +
						", You are exiting kickstarter. Bye!\n]", io.getMessages().toString());
	}
	
	@Test
	public void fake_with_projects_witout_detailed_description() throws IOException {
		
		FakeIO io = new FakeIO("1","1","0","0","0");//chose 1st category, chose first project, go back, go back, go out of program
		View view = new ConsoleView(io);
		
		quotes.add(new Quote("testing quote"));
		Category name1 = new Category("name1");
		Category name2 = new Category("name2");
		categories.add(name1);//the first test for fake is done with empty containers!!!
		categories.add(name2);
		projects.add(new Project("test1", "testing1", name1, 10000, 50));
		projects.add(new Project("test2", "testing2", name1, 20, 50));
		projects.add(new Project("test3", "testing3", name2, 3000, 50));
		
		Kickstarter kickstarter = new Kickstarter(io, storage, view);
		
		kickstarter.run();
		
		assertEquals("[Quote: testing quote, ----------Categories----------\n" +
				"1 - name1\n" +
				"2 - name2\n" +
				"---------------------------------, \n" + 
				"Please select your variant (0 to goback)--> \n" +
				", --------Welcome to projects--------\n" +
				"1 - test1\n" +
				"    Description = testing1\n" +
				"    TotalAmount/CollectAmount   10000/0.0\n" +
				"    DaysLeft = 50\n" +
				"    Category : name1\n" +
				"2 - test2\n" +
			    "    Description = testing2\n" +
			    "    TotalAmount/CollectAmount   20/0.0\n" +
			    "    DaysLeft = 50\n" +
			    "    Category : name1\n" +
			"---------------------------------, \n" + 
			"Please select your variant (0 to goback)--> \n" + 
			", --------Welcome to project you have chosen--------\n"+
			"1 - test1\n" +
			    "    Description = testing1\n" +
			    "    TotalAmount/CollectAmount   10000/0.0\n" +
			    "    DaysLeft = 50\n" +
			    "    Category : name1\n" +
			    "    Link : \n" + 
			    "    Question and answers : \n" + 
			    	", \n" + 
			    	"Please enter 0 to go back or 1 to invest in project --> \n" + 
			    	", --------Welcome to projects--------\n" +
			    	"1 - test1\n" +
			    	    "    Description = testing1\n" +
			    	    "    TotalAmount/CollectAmount   10000/0.0\n" +
			    	    "    DaysLeft = 50\n" +
			    	    "    Category : name1\n" +
			    	"2 - test2\n" +
			    	    "    Description = testing2\n" +
			    	    "    TotalAmount/CollectAmount   20/0.0\n" +
			    	    "    DaysLeft = 50\n" +
			    	    "    Category : name1\n" +
			    	"---------------------------------, \n" + 
			    	"Please select your variant (0 to goback)--> \n" + 
			    	", Quote: testing quote, ----------Categories----------\n" +
			    	"1 - name1\n" +
			    	"2 - name2\n" +
			    	"---------------------------------, \n" +
			    	"Please select your variant (0 to goback)--> \n" +
			    	", You are exiting kickstarter. Bye!\n]", io.getMessages().toString());
	}
	
	@Test
	public void mockTest() throws IOException {
		//given
//				FakeIO io = new FakeIO(1,0,0);//choose category, go back, go back and exit the program
			IO io = mock(IO.class);		
			QuoteContainer quotes = mock(QuoteContainer.class);
				
			Storage storage = new Storage(quotes, categories, projects);
			View view = new ConsoleView(io);
			
			categories.add(new Category("name1"));
			categories.add(new Category("name2"));
			
			Kickstarter kickstarter = new Kickstarter(io, storage, view);
				
			//when
			when(io.readline()).thenReturn("1","0","0");
			when(quotes.getRandom()).thenReturn(new Quote("testing quote"));
			
			kickstarter.run();
				
			//then
			verify(io, times(2)).show("Quote: testing quote");
			verify(io, times(2)).show("----------Categories----------\n1 - name1\n2 - name2\n"
					                                      + "---------------------------------");
			verify(io, times(3)).showinline("\nPlease select your variant (0 to goback)--> ");
			verify(io).show("No projects exist for chosen category.\n");
			verify(io).showinline("You are exiting kickstarter. Bye!");
//			validateMockitoUsage();
	}
	
	@Test
	public void TestWrongInputFromUser() throws IOException {
		//given
		CheckStringDecoratorTest io = new CheckStringDecoratorTest(new FakeIO("xxx","0"));//choose category, go back, go back and exit the program
		View view = new ConsoleView(io);
		
		categories.add(new Category("name1"));//the first test for fake is done with empty containers!!!
		categories.add(new Category("name2"));
		quotes.add(new Quote("testing quote"));
		
		Kickstarter kickstarter = new Kickstarter(io, storage, view);
		
		//when
		kickstarter.run();
		
		//then
		assertEquals("[Quote: testing quote, ----------Categories----------\n" +
				"1 - name1\n" + 
				"2 - name2\n"+ 
				"---------------------------------, \n" + 
				"Please select your variant (0 to goback)--> \n" + 
				", What are you entering? Only numbers from the list or 0 --> \n" + 
				", You are exiting kickstarter. Bye!\n]", io.getMessages().toString());
	}
	
	@Test
	public void TestWrongCategoryIdOutOfRange() throws IOException {
		//given
		CheckStringDecoratorTest io = new CheckStringDecoratorTest(new FakeIO("3","0"));//choose category, go back, go back and exit the program
		View view = new ConsoleView(io);
		
		categories.add(new Category("name1"));//the first test for fake is done with empty containers!!!
		categories.add(new Category("name2"));
		quotes.add(new Quote("testing quote"));
		
		Kickstarter kickstarter = new Kickstarter(io, storage, view);
		
		//when
		kickstarter.run();
		
		//then
		assertEquals("[Quote: testing quote, ----------Categories----------\n" +
				"1 - name1\n" + 
				"2 - name2\n"+ 
				"---------------------------------, \n" + 
				"Please select your variant (0 to goback)--> \n" + 
				", ******************************, You put incorrect id. try again, Quote: testing quote, ----------Categories----------\n" + 
				"1 - name1\n" +
				"2 - name2\n" +
				"---------------------------------, \n" +
				"Please select your variant (0 to goback)--> \n"  + 
				", You are exiting kickstarter. Bye!\n]", io.getMessages().toString());
	}
	
	@Test
	public void Mock_Invest_Into_Project() throws IOException {
		IO io = mock(IO.class);		
		QuoteContainer quotes = mock(QuoteContainer.class);
			
		Storage storage = new Storage(quotes, categories, projects);
		View view = new ConsoleView(io);
		
		Category name1 = new Category("name1");
		Category name2 = new Category("name2");
		categories.add(name1);
		categories.add(name2);
		projects.add(new Project("test1", "testing1", name1, 10000, 50));
		projects.add(new Project("test2", "testing2", name1, 20, 50));
		projects.add(new Project("test3", "testing3", name2, 3000, 50));
		
		Kickstarter kickstarter = new Kickstarter(io, storage, view);
			
		//when
		when(io.readline()).thenReturn("1","2","1", "vova", "112", "123","0", "0", "0");
		when(quotes.getRandom()).thenReturn(new Quote("testing quote"));
		
		kickstarter.run();
			
		//then
		verify(io).show("--------Donations--------\n");
		verify(io).show("Your name -- > ");
		verify(io).show("Your card id -- >");
		verify(io).show("Sum -- >");
		verify(io).show("Thank you for investing.");
	}
}
