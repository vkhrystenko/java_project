package kickstarter.kickstarter;

import java.util.Random;

import container.inFile.CategoriesInFile;
import container.inList.ProjectContainerInList;
import container.inList.QuoteContainerInList;

public class KickstarterInFileTest extends KickstarterTest {

	@Override
	CategoriesInFile getCategories() {
		return new CategoriesInFile("test-categories.txt");
	}

	@Override
	ProjectContainerInList getProjects() {
		return new ProjectContainerInList();
	}

	@Override
	QuoteContainerInList getQuotes() {
		return new QuoteContainerInList(new Random());
	}
}
