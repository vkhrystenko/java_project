package kickstarter.kickstarter;

import java.util.Random;

import container.inList.CategoryInList;
import container.inList.ProjectContainerInList;
import container.inList.QuoteContainerInList;

public class KickstarterInListTest extends KickstarterTest {
	
	@Override
	CategoryInList getCategories() {
		return new CategoryInList();
	}

	@Override
	ProjectContainerInList getProjects() {
		return new ProjectContainerInList();
	}

	@Override
	QuoteContainerInList getQuotes() {
		return new QuoteContainerInList(new Random());
	}	
}
