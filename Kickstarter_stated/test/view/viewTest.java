package view;

import org.junit.Test;

import views.ConsoleView;
import container.QuoteContainer;
import container.inList.QuoteContainerInList;
import data.Quote;
import IO.IO;
import static org.mockito.Mockito.*;

public class viewTest {
	
	@Test
	public void testQuotes() {
		IO io = mock(IO.class);
		ConsoleView view1 = new ConsoleView(io);
	
		QuoteContainer quotes = mock(QuoteContainerInList.class);
	
		when(quotes.getRandom()).thenReturn(new Quote("testing quote"));
		view1.processQuotes(quotes);	
		
		verify(io).show("Quote: testing quote");
		validateMockitoUsage();
	}
}
