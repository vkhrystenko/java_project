package views;

import data.Category;

public class CategoryView implements ViewData<Category>{
	
	@Override
	public String getDescription(Category category) {
		return category.getId() + " - " + category.getName() + "\n";
	}

	@Override
	public String getDetailedDescription(Category category) {
		return getDescription(category);
	}
}
