package views;

import data.Data;

public interface ViewData <T extends Data>{
	String getDescription(T content);
	String getDetailedDescription(T content);
}
