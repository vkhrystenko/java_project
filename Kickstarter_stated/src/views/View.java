package views;

import java.io.IOException;
import java.util.List;

import container.QuoteContainer;
import data.Category;
import data.Project;
import IO.CheckStringDecorator;
import IO.IO;

public abstract class View {
	CategoryView categoryview = new CategoryView();
	QuoteView quoteview = new QuoteView();
	ProjectView projectview = new ProjectView();
	IO io;
	IO io_integered;
	
	public View(IO io) {
		this.io = io;
		this.io_integered = new CheckStringDecorator(io);
	}
	
	public void processQuotes(QuoteContainer quotes) {
		try {
			io_integered.show(quoteview.getDescription(quotes.getRandom()));
		} catch (IllegalArgumentException e) {
			io_integered.show("---***---");
		}
	}
	
	public void processCategories(List<Category> categories) {
		StringBuilder result = new StringBuilder("----------Categories----------\n");
		for( Category cat : categories) {
			result.append(categoryview .getDescription(cat));
		}
		result.append("---------------------------------");
		io_integered.show(result.toString());
	}
	
	public void processProjects(List<Project> projects) {
		if(projects.isEmpty()) {
			io_integered.show("No projects exist for chosen category.\n");
		} else {
			StringBuilder result = new StringBuilder("--------Welcome to projects--------\n");
			for( Project proj : projects) {
				result.append(projectview.getDescription(proj));
			}
			result.append("---------------------------------");
			io_integered.show(result.toString());
			}
	}
	
	public void processSpecificProject(Project project) {
		StringBuilder result = new StringBuilder("--------Welcome to project you have chosen--------\n");
		result.append(projectview.getDetailedDescription(project));
		io_integered.show(result.toString());
	}
	
	public void processDonations(Project project) {
		io.show("--------Donations--------\n");
		io.show("Your name -- > ");
		String name = null;
		try {
			name = io.readline();
		} catch (IOException e) {
			e.printStackTrace();
		}
		project.donation.setDonatorName(name);
		io.show("Your card id -- >");
		int cardid = 0;
		try {
			cardid = Integer.parseInt(io.readline());
		} catch (NumberFormatException | IOException e) {
			e.printStackTrace();
		}
		project.donation.setCardId(cardid);
		
		io.show("Sum -- >");
		int sum = 0;
		try {
			sum = Integer.parseInt(io.readline());
		} catch (NumberFormatException | IOException e) {
			e.printStackTrace();
		}
		project.donate(sum);
		io.show("Thank you for investing.");
	}
	
	public void handleWrongId() {
		io.show("******************************");
		io.show("You put incorrect id. try again");
	}
	public void showfooter(String string) {
		io.showinline(string);
	}

}
