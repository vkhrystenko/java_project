package views;

import data.Quote;

public class QuoteView implements ViewData<Quote> {
	
	@Override
	public String getDescription(Quote quote) {
		return "Quote: " + quote.getName();
	}
	
	public String getDetailedDescription(Quote quote) {
		return getDescription(quote);
	}

}
