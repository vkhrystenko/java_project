package views;

import data.Project;

public class ProjectView implements ViewData<Project> {
	
	@Override
	public String getDescription(Project project) {
		StringBuilder projectdescription = new StringBuilder();
		
		projectdescription.append(project.getId() +
									" - " + project.getName() + "\n" +
									"    Description = " + project.getDescription() + "\n" +
									"    TotalAmount/CollectAmount   " + project.donation.getTotalAmount() + "/" + 
									project.donation.getCollectAmount() + "\n" +
									"    DaysLeft = " + project.donation.getDaysLeft() + "\n" +
									"    Category : " + project.getCategory().getName() + "\n");
		
		return projectdescription.toString();
	}
	
	@Override
	public String getDetailedDescription(Project project) {
		StringBuilder projectdetaileddescription = new StringBuilder();
		
		projectdetaileddescription.append(getDescription(project) +
										"    Link : " + project.getLink() + "\n" +
										"    Question and answers : " + project.getQuestionsAndAnswers() + "\n");
		
		return projectdetaileddescription.toString();
	}
}
