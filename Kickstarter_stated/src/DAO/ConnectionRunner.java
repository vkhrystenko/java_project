package DAO;

import java.sql.Connection;
import java.sql.SQLException;

public interface ConnectionRunner<T> {
	T run(Connection connection) throws SQLException;
}
