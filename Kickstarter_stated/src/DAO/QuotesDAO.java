package DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import container.QuoteContainer;
import data.Data;
import data.Quote;

public class QuotesDAO extends ContainersDAO<Quote> implements QuoteContainer{
	
	@Override
	public Quote get(int index) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Quote getById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void add(Quote content) {
		table = content.getClass().getSimpleName().toLowerCase();
		tableincolumns = table;
		
		id = content.getId();
		
		setColumn("id", id);
		setColumn("name", content.getName());
		
		try {
			insert();
		} catch (SQLException e ) {
			throw new RuntimeException("Error was received from database while inserting quote", e);
		}
		clearFields();
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<Quote> getContainer() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Quote getRandom() {
		try {
			Statement random_query = connection.createStatement();
			ResultSet return_data = random_query.executeQuery(RANDOM_QUOTE_QUERY);
            
            if(return_data.next()) {    
                    String result = (String) return_data.getObject(1);
                    
                    return_data.close();
                    random_query.close();
                    
                    return new Quote(result);
            } else {
            	return new Quote("");
            }
		} catch (SQLException e) {
			throw new RuntimeException("Can not create statement", e);
		}
		
	}

	@Override
	public List<Quote> getContainer(Data selectedchoice) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public void truncate() {
		try {
			PreparedStatement delete_query = connection.prepareStatement(String.format(TRUNCATE_QUERY, "quotes"));
			delete_query.execute();
		} catch (SQLException e) {
			throw new RuntimeException("Error was received from database while truncating tables", e);
		}
	}

}
