package container;

import java.util.List;

import data.Data;

public interface Container<T extends Data> {
	public T get(int index);
	public T getById(int id);	
	public void add(T content);
	public int size();
	public List<T> getContainer(Data selectedchoice);
	public List<T> getContainer();
	public void truncate();
}

