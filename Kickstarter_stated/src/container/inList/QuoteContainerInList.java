package container.inList;

import java.util.List;
import java.util.Random;

import container.QuoteContainer;
import data.Data;
import data.Quote;

public class QuoteContainerInList extends ContainersInList<Quote> implements QuoteContainer {
	private Random random;
	
	public QuoteContainerInList(Random random) {
		this.random = random;
	}
	
	@Override
	public Quote getRandom() {
		int randomIndex = random.nextInt(size());
		return container.get(randomIndex);
	}

	@Override
	public List<Quote> getContainer(Data selectedchoice) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void truncate() {
		// TODO Auto-generated method stub
	}
}
