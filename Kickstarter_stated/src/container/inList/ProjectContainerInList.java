package container.inList;

import java.util.ArrayList;
import java.util.List;

import data.Data;
import data.Project;

public class ProjectContainerInList extends ContainersInList<Project> {

	@Override
	public List<Project> getContainer(Data category) {
		List<Project> newcontainer = new ArrayList<Project>();
		
		for(Project project : getContainer() ) {
			if(project.getCategory().equals(category)) {
				newcontainer.add(project);
			}
		}
		return newcontainer;
	}

	@Override
	public void truncate() {
		// TODO Auto-generated method stub
	}
}