package container;

import container.inList.ProjectContainerInList;
import data.Category;
import data.Project;

public class CategoryProjectBinderFileandList extends CategoryProjectBinder {
	
	public CategoryProjectBinderFileandList() {
		newcontainer = new ProjectContainerInList();	
	}
	
	public Container<Project> getListOfProjects(Category category, Container<Project> projectstorage) {
		for(Project project : projectstorage.getContainer() ) {
			if(project.getCategory().equals(category)) {
				newcontainer.add(project);
			}
		}
		return newcontainer;
	}
}
