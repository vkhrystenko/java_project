package container.inFile;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import container.Container;
import data.Data;

public abstract class ContainersInFile<T extends Data> implements Container<T> {

	
	protected File file;
	BufferedReader in = null;
	BufferedWriter out = null;

	public ContainersInFile(String filename) {
		file = createFileIfDoesNotExist(filename);
	}
	@Override
	public T get(int index) {
		return getContainer().get(index);
	}

	@Override
	public T getById(int id) {
		for(T item : getContainer()) {
			if(item.getId() == id) {
				return item;
			}
		}
		throw new IndexOutOfBoundsException();
	}

	@Override
	public void add(T content) {
		try {
			in = new BufferedReader(new FileReader(file));
			String line = in.readLine();
			while(line != null) {
				if(line.equalsIgnoreCase(content.getName())) {
					return;
				}
				line = in.readLine();
			}
			out =  new BufferedWriter(new FileWriter(file,true));
			out.append(content.getName() + "\n");
		} catch (FileNotFoundException e) {
			handleException("Was not able to open file!!!");
		} catch (IOException e) {
			handleException("There were problems with writing file!!!");
		} finally {
				handleFinallyWriter(out);
				handleFinallyReader(in);
			}
		
	}
	
	protected void handleFinallyWriter(BufferedWriter out) {
		if(out != null) {
			try {
				out.close();
			} catch (IOException e) {
				handleException("Was not able to close file!!!");
			}
}
	}
	protected void handleException(String string) {
		throw new RuntimeException(string);
	}

	@Override
	public int size() {	
		int counter = 0;
		try {
			in = new BufferedReader(new FileReader(file));
			while(in.readLine() != null) {
				counter += 1;
			}
			return counter;
		} catch (FileNotFoundException e) {
			handleException("Was not able to open file!!!");
		} catch (IOException e) {
			handleException("There were problems with reading file!!!");
		} finally {
				handleFinallyReader(in);
		}
		return counter;
	}
	
	protected void handleFinallyReader(BufferedReader in) {
		if(in != null) {
			try {
				in.close();
			} catch (IOException e) {
				handleException("There were problems with closing file!!!");
			}
}
	}

	private File createFileIfDoesNotExist(String filename) {
		File file = new File(filename);
		if(!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				handleException("There were problems while creating file!!!");
			}
		}
		return file;
	}

	@Override
	public abstract List<T> getContainer();
}

