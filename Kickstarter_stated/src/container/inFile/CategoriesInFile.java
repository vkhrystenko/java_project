package container.inFile;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import data.Category;
import data.Data;

public class CategoriesInFile extends ContainersInFile<Category> {

	public CategoriesInFile(String filename) {
		super(filename);
		BufferedWriter out = null;
		try {
			out = new BufferedWriter(new FileWriter(file,false));
			out.write("");
		} catch (IOException e) {
			handleException("There were problems with writing file!!!");
		} finally {
			handleFinallyWriter(out);
		}
	}

	@Override
	public List<Category> getContainer() {
		Category.categoryid = 0;
		List<Category> list= new ArrayList<Category>();
		try {
			in = new BufferedReader(new FileReader(file));
			String line = in.readLine();
			while(line != null) {
				list.add(new Category(line));
				line = in.readLine();
			}
			Category.categoryid = 0;
			
			return list;
		} catch (FileNotFoundException e) {
			handleException("Was not able to open file!!!");
		} catch (IOException e) {
			handleException("There were problems with reading file!!!");
		} finally {
			handleFinallyReader(in);
		}
		return list;
	}

	@Override
	public List<Category> getContainer(Data selectedchoice) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void truncate() {
		// TODO Auto-generated method stub
	}
}
