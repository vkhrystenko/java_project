package States;

import kickstarter.Kickstarter;
import views.View;
import data.Data;

public abstract class State {
	Kickstarter kickstarter;
	View view;

	public State(Kickstarter kickstarter) {
		this.kickstarter = kickstarter;
		view = kickstarter.getView();
	}
	
	abstract public void runPage();
	abstract public void goState();
	abstract public void goBackState();
//	abstract public void setContainer(Data item);
	abstract public Data getItem(int id);
	public void handleWrongId() {
		view.handleWrongId();
	}
	abstract public void setItem(Data item);
}
