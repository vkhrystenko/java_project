package States;

import data.Data;
import data.Project;
import kickstarter.Kickstarter;

public class InvestMoney extends State {
	private Project project;
	
	public InvestMoney(Kickstarter kickstarter) {
		super(kickstarter);
	}

	@Override
	public void runPage() {
		view.processDonations(project);
		view.showfooter("\nPlease select your variant (0 to goback)--> ");
	}

	@Override
	public void goState() {
		throw new UnsupportedOperationException("No further state");	
	}

	@Override
	public void goBackState() {
		kickstarter.getPageSwitcher().setState(kickstarter.getPageSwitcher().getProjectPage());
	}

	@Override
	public Data getItem(int id) {
		throw new UnsupportedOperationException("No further processing");
	}

	@Override
	public void setItem(Data item) {
		project = (Project) item;
	}

}
