package States;

import kickstarter.Kickstarter;
import data.Data;

public class StartPage extends State {

	public StartPage(Kickstarter kickstarter) {
		super(kickstarter);
	}
	
	@Override
	public void runPage() {
		view.processQuotes(kickstarter.storage.quotes);
		view.processCategories(kickstarter.storage.categories.getContainer());
		view.showfooter("\nPlease select your variant (0 to goback)--> ");
	}

	@Override
	public void setItem(Data item) {
		;
	}
	
	@Override
	public void goState() {
		kickstarter.getPageSwitcher().setState(kickstarter.getPageSwitcher().getProjectsPage());
	}
	
	public void goBackState() {
		kickstarter.getPageSwitcher().setState(kickstarter.getPageSwitcher().getEndPage());
	}

	public Data getItem(int id) {
		return kickstarter.storage.categories.getById(id);
	}
}
