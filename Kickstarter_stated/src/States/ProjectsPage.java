package States;

import java.util.List;

import data.Data;
import data.Project;
import kickstarter.Kickstarter;

public class ProjectsPage extends State {
	private List<Project> newcontainer;
	
	public ProjectsPage(Kickstarter kickstarter) {
		super(kickstarter);
	}

	@Override
	public void runPage() {
//		System.out.println("this " + newcontainer.toString());
		view.processProjects(newcontainer);
		view.showfooter("\nPlease select your variant (0 to goback)--> ");
	}

	@Override
	public void goState() {
		kickstarter.getPageSwitcher().setState(kickstarter.getPageSwitcher().getProjectPage());
	}

	@Override
	public void goBackState() {
		kickstarter.getPageSwitcher().setState(kickstarter.getPageSwitcher().getStartPage());
	}

	@Override
	public void setItem(Data item) {
		newcontainer = kickstarter.storage.projects.getContainer(item);
	}

	@Override
	public Data getItem(int id) {
		for( Project proj: newcontainer) {
			if(proj.getId()==id) {
				return proj;
			}
		}
		throw new IndexOutOfBoundsException();
	}

}
