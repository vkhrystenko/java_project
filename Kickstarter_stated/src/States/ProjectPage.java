package States;

import data.Data;
import data.NullData;
import data.Project;
import kickstarter.Kickstarter;

public class ProjectPage extends State {
	private Project project;
	
	public ProjectPage(Kickstarter kickstarter) {
		super(kickstarter);
	}

	@Override
	public void runPage() {
		view.processSpecificProject(project);
		view.showfooter("\nPlease enter 0 to go back or 1 to invest in project --> ");
	}

	@Override
	public void goState() {
		kickstarter.getPageSwitcher().setState(kickstarter.getPageSwitcher().getInvestMoney());
	}

	@Override
	public void goBackState() {
		kickstarter.getPageSwitcher().setState(kickstarter.getPageSwitcher().getProjectsPage());
	}

	@Override
	public void setItem(Data item) {
		project = (Project) item;
	}

	@Override
	public Data getItem(int id) {
		if(id == 1) {
			view.showfooter("Thank you for insesting in project :-)\n");
			return project;
		}
		throw new IndexOutOfBoundsException();
	}

}
