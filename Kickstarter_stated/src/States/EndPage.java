package States;

import data.Data;
import data.NullData;
import kickstarter.Kickstarter;

public class EndPage extends State {
	public static final Data nullData = new NullData();
	
	public EndPage(Kickstarter kickstarter) {
		super(kickstarter);
	}

	@Override
	public void runPage() {
		view.showfooter("You are exiting kickstarter. Bye!");
	}

	@Override
	public void goState() {
		;
	}

	@Override
	public void goBackState() {
		;
	}

	@Override
	public void setItem(Data item) {
		;
	}

	@Override
	public Data getItem(int id) {
		return nullData;
	}

}
