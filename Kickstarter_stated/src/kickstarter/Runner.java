package kickstarter;

import java.io.IOException;
import java.util.Random;

import views.ConsoleView;
import DAO.CategoriesDAO;
import DAO.ContainersDAO;
import DAO.ProjectsDAO;
import DAO.QuotesDAO;
import IO.CheckStringDecorator;
import IO.ConsoleIO;
import IO.IO;
import container.Container;
import container.QuoteContainer;
import container.inList.CategoryInList;
import container.inList.ProjectContainerInList;
import data.Category;
import data.Project;
import data.Quote;
import container.inList.QuoteContainerInList;


public class Runner {
	public static void main(String[] args) throws IOException {
//		Container<Quote> quotes = new QuoteContainerInList(new Random());
//		quotes.add(new Quote("Shit happens"));
//		quotes.add(new Quote("Dont worry, be happy"));
//		quotes.add(new Quote("To be or not to be"));
//
//		Category sport = new Category("Sport");
//		Category art = new Category("Art");
//		Category test = new Category("art2");
//		Container<Category> categories = new CategoryInList();
//		categories.add(sport);
//		categories.add(art);
//		categories.add(test);
//		
//		Project project1 = new Project("sport arena", "arena in Kiev", sport, 10000, 50);
//		project1.donate(40);
//		project1.setLink("www.project1.com");
//		project1.addQuestionAnswer("why1");
//
//		Project project2 = new Project("cybersystem", "cybersystem skynet", sport, 100000, 200);
//		project2.donate(500);
//		project2.setLink("www.project2.com");
//		project2.addQuestionAnswer("why2");
//
//		Project project3 = new Project("French art nano", "big badaboom", art, 1000000, 1000);
//		project3.donate(7000);
//		project3.setLink("www.project3.com");
//		project3.addQuestionAnswer("why3");
//		
//		Container<Project> projects = new ProjectContainerInList();
//		projects.add(project1);
//		projects.add(project2);
//		projects.add(project3);
//		
//		Storage storage = new Storage((QuoteContainer)quotes, categories, projects);
//		IO io = new CheckStringDecorator(new ConsoleIO());
//		ConsoleView view = new ConsoleView(io);
		
//		---------------------------------------------------------DB Code
		ContainersDAO<Quote> quotes = new QuotesDAO();
		quotes.add(new Quote("Shit happens"));
		quotes.add(new Quote("Dont worry, be happy"));
		quotes.add(new Quote("To be or not to be"));
	
		Category sport = new Category("Sport");
		Category art = new Category("Art");
		Category test = new Category("art2");
		
		ContainersDAO<Category> categories = new CategoriesDAO();
		categories.add(sport);
		categories.add(art);
		categories.add(test);
		
		
		Project project1 = new Project("sport arena", "arena in Kiev", sport, 10000, 50);
		project1.donate(40);
		project1.setLink("www.project1.com");
		project1.addQuestionAnswer("why1");
		
		Project project2 = new Project("cybersystem", "cybersystem skynet", sport, 100000, 200);
		project2.donate(500);
		project2.setLink("www.project2.com");
		project2.addQuestionAnswer("why2");

		Project project3 = new Project("French art nano", "big badaboom", art, 1000000, 1000);
		project3.donate(7000);
		project3.setLink("www.project3.com");
		project3.addQuestionAnswer("why3");
		
		ContainersDAO<Project>  projects  = new ProjectsDAO();
		projects.add(project1);
		projects.add(project2);
		projects.add(project3);
		
		Storage storage = new Storage((QuoteContainer)quotes, categories, projects);
		IO io = new ConsoleIO();
		ConsoleView view = new ConsoleView(io);
		
		Kickstarter kickstarter = new Kickstarter(io, storage, view);
		kickstarter.run();
		categories.truncate();
		quotes.truncate();
		projects.truncate();
	}
}
