package kickstarter;

import container.Container;
import container.QuoteContainer;
import data.Category;
import data.Project;

public class Storage {
	public Container<Category> categories;
	public Container<Project> projects;
	public QuoteContainer quotes;
	
	public Storage (QuoteContainer quotes, Container<Category> categories, Container<Project> projects) {
		this.categories = categories;
		this.quotes = quotes;
		this.projects = projects;
	}
}
