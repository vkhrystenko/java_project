package kickstarter;

import java.io.IOException;

import views.View;
import data.Data;
import IO.CheckStringDecorator;
import IO.IO;

public class Kickstarter {
	public IO inputoutput;
	public Storage storage;
	public View view;
	
	private PageSwitcher pageswitcher;
	
	public Kickstarter(IO io, Storage storage, View view) {
		this.inputoutput = new CheckStringDecorator(io);
		this.storage = storage;
		this.view = view;
		pageswitcher = new PageSwitcher(this);
	}
	
	public void run() throws IOException {
		int id = 0;
		while(true) {
			pageswitcher.getCurrentState().runPage();
			if(pageswitcher.getCurrentState() == pageswitcher.getEndPage()) {
				break;
			}
			
			id = Integer.parseInt(chooseItem());
			
			if (id == CloseKickstarter.EXIT0.getId()) {
				pageswitcher.getCurrentState().goBackState();
			} else {
				try {
					Data item = pageswitcher.getCurrentState().getItem(id);
					pageswitcher.getCurrentState().goState();
					pageswitcher.getCurrentState().setItem(item);
				} catch (Exception e) {
//					view.showfooter(e.toString() + "\n");
					pageswitcher.getCurrentState().handleWrongId();
			}
		}
				}
	}

	
	private String chooseItem() throws IOException {
		return inputoutput.readline();
	}

	public void setView(View view) {
		this.view = view;
	}
	
	public View getView() {
		return view;
	}

	public PageSwitcher getPageSwitcher() {
		return pageswitcher;
	}
}
