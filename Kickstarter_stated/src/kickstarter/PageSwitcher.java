package kickstarter;

import States.EndPage;
import States.InvestMoney;
import States.ProjectPage;
import States.ProjectsPage;
import States.StartPage;
import States.State;

public class PageSwitcher {
	Kickstarter kickstarter;
	
	private State startpage;
	private State projectspage;
	private ProjectPage projectpage;
	private State endpage;
	private State investmoney;
	
	private State state;

	
	public PageSwitcher(Kickstarter kickstarter) {
		this.kickstarter = kickstarter;
		startpage = new StartPage(kickstarter);
		projectspage = new ProjectsPage(kickstarter);
		projectpage = new ProjectPage(kickstarter);
		endpage = new EndPage(kickstarter);
		investmoney = new InvestMoney(kickstarter);
		state = new StartPage(kickstarter);
	}
	
	public State getEndPage() {
		return endpage;
	}
	public State getStartPage() {
		return startpage;
	}
	
	public State getProjectsPage() {
		return projectspage;
	}
	
	public State getProjectPage() {
		return projectpage;
	}
	
	public void setState(State newstate) {
		state = newstate;
	}
	
	public State getCurrentState() {
		return state;
	}

	public State getInvestMoney() {
		return investmoney;
	}
}
