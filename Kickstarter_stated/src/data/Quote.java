package data;

public class Quote implements Data{
	public static int quoteid = 0;
	
	private int id;
	private String quote;

	public Quote(String quote) {
		if (quote == null) {
			throw new IllegalArgumentException();
		}
		this.id = ++quoteid;
		this.quote = quote;
	}
	
	public Quote(int id, String quote) {
		this(quote);
		this.id = id;
	}
	
	@Override
	public int getId() {
		return id;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((quote == null) ? 0 : quote.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Quote other = (Quote) obj;
		if (quote == null) {
			if (other.quote != null)
				return false;
		} else if (!quote.equals(other.quote))
			return false;
		return true;
	}

	@Override
	public String getName() {
		return quote;
	}
}
