package data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Project implements Data {
	public static int projectid = 0;
	
	private int id;
	
	private String name;
	private String description;
	
	Connection connection = null;
	private Category category;
	
	public DonationLogic donation;

	private String link;
	private String questionsAndAnswers;
	
	public Project(String name, String description, Category category, int totalAmount, int daysLeft) {
		if (name == null || description == null || category == null || totalAmount <= 0 || daysLeft <= 0) {
			throw new IllegalArgumentException();
		}
		this.category = category;
		this.id = ++projectid;
		this.name = name;
		this.description = description;
		this.category = category;
		donation = new DonationLogic(totalAmount, daysLeft);
		this.link = "";
		this.questionsAndAnswers = "";
	}
	
	public Project(int id, String name, String description, Category category, int totalAmount, int daysLeft) {
		this(name, description, category, totalAmount, daysLeft);
		this.id = id;
	}
	
	public Project(int id, String name, String description, Category category, int totalAmount, int daysLeft, double collectedamount) {
		this(id, name,description,category,totalAmount, daysLeft);
		this.donation.setCollectedAmount(collectedamount);
	}
	
	public Project(int id, String name,
			String description, Category category, int totalamount,
			int daysleft, double collectedamount, Connection connection) {
		this(id, name,description,category,totalamount, daysleft, collectedamount);
		this.connection = connection;
	}

	public void setLink(String link) {
		if (link == null) {
			throw new IllegalArgumentException();
		}
		this.link = link;
	}
	
	public void setCategory(Category category) {
		this.category = category;
	}
	
	public void addQuestionAnswer(String questionAndAnswers) {
		if (questionAndAnswers == null) {
			throw new IllegalArgumentException();
		}
		this.questionsAndAnswers = questionAndAnswers;
	}
	
	public void donate(double amount) {
		donation.donate(amount);
		if(connection != null) {
			String UPDATE_DONATION_QUERY = "UPDATE donations SET donation_collect_amount = %1$s where  donation_project_id = ?";
			try {
				PreparedStatement update_query = connection.prepareStatement(String.format(UPDATE_DONATION_QUERY, donation.getCollectAmount()));
				update_query.setInt(1, id);
				update_query.executeUpdate();
			} catch (SQLException e) {
				System.out.println("Error while updating DB!!!");
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public int getId() {
		return id;
	}

	public Category getCategory() {
		return category;
	}
	
	public DonationLogic getDonation() {
		return donation;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public String getLink() {
		return link;
	}

	public String getQuestionsAndAnswers() {
		return questionsAndAnswers;
	}
}
