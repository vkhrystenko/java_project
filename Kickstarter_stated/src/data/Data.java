package data;

public interface Data {
	int getId();
	String getName();
}
