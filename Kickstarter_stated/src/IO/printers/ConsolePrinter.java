package IO.printers;

public class ConsolePrinter implements Printer{
	public void show(String string) {
		System.out.println(string);
	}
	
	public void showinline(String string) {
		System.out.print(string);
	}
}
