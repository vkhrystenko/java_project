package IO.printers;

public interface Printer {
	void show(String string);
	void showinline(String string);
}
