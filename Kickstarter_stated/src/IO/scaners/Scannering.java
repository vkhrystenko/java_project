package IO.scaners;

import java.io.IOException;

public interface Scannering {
	String readline() throws IOException;
}
