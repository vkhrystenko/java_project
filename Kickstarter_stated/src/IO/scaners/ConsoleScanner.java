package IO.scaners;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ConsoleScanner implements Scannering {
	
	@Override
	public String readline() throws IOException {
		BufferedReader reader = new BufferedReader(
									new InputStreamReader(System.in));
		return reader.readLine();
	}
}

