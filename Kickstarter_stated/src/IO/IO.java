package IO;

import IO.printers.Printer;
import IO.scaners.Scannering;

public interface IO extends Printer, Scannering {}
