package IO;

import java.io.IOException;

public class DecoratorIO implements IO {
	IO io;
	
	public DecoratorIO(IO io) {
		this.io = io;
	}
	
	@Override
	public void show(String string) {
		io.show(string);
	}

	@Override
	public void showinline(String string) {
		io.showinline(string);
	}

	@Override
	public String readline() throws IOException {
		return io.readline();
	}
	
	
}
