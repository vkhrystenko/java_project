package IO;

import java.io.IOException;

import IO.printers.ConsolePrinter;
import IO.scaners.ConsoleScanner;

public class ConsoleIO implements IO {
	ConsolePrinter printer = new ConsolePrinter();
	ConsoleScanner scaner = new ConsoleScanner();
	
	@Override
	public void show(String string) {
		printer.show(string);
	}

	@Override
	public void showinline(String string) {
		printer.showinline(string);
	}

	@Override
	public String readline() throws IOException {
		return scaner.readline();
	}

}
