package IO;

import java.io.IOException;

public class CheckStringDecorator extends DecoratorIO {
	
	
	public CheckStringDecorator(IO io) {
		super(io);
	}

	@Override
	public String readline() throws IOException {
		String input;
		while(true) {
			input = io.readline();
				try {
					Integer.parseInt(input);
					break;
				} catch (NumberFormatException e) {
					io.showinline("What are you entering? Only numbers from the list or 0 --> ");
				}
		}
		return input;
	}
}
