<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
	<head>
		<title>Kickstarter projects</title>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/styles/projects.css" />
	</head>
	<body bgcolor=white>
	  <c:choose>
		<c:when test="${empty sessionScope.projects}">
			No projects for chosen category.
		</c:when>
		<c:otherwise>
				<c:forEach var="project" items="${projects}">
				  <table>
   					<tr>
   						<td>ID:</td> 
   						<td><c:out value="${project.id}" /></td>
   					</tr>
   					<tr>
   						<td>Name: </td>
   						<td><c:out value="${project.name}"/></td>
   					</tr>
   					<tr>
   						<td>Description: </td>
   						<td><c:out value="${project.description}"/></td>
   					</tr>
   					<tr>
   						<td>TotalAmount/CollectAmount: </td>
   						<td><c:out value="${project.donation.totalAmount}/${project.donation.collectAmount}"/></td>
   					</tr>
   					<tr>
   						<td>DaysLeft: </td>
   						<td><c:out value="${project.donation.daysLeft}"/></td>
   					</tr>
   					<tr>
   						<td>Category: </td>
   						<td><c:out value="${project.category.name}"/></td>
   					</tr>
   				  </table>
				</c:forEach>
		</c:otherwise>
	  </c:choose>
	
	  <c:if test="${ not empty error }">
			Error: <c:out value="${sessionScope.error}" />
			<c:remove var="error" scope="session" />
	  </c:if>
	  <c:choose>
	  	<c:when test="${ not empty sessionScope.projects}">
	  		<form action="/kickstarter/project" method="get">
				<label for="variant">Please select your variant (enter number of project for details)</label>
				<input id="variant" name="projectid" size="2"/>
				<input type="submit" value="Go!">
	  		</form>
	  	</c:when>
	  </c:choose>
	</body>

</html>

