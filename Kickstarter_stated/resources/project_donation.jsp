<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<html>
	<head>
		<title>Kickstarter project donation</title>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/styles/donation.css" />
		
		<script>
			function confirmInput() {
				var projectname = "${sessionScope.project.name}";
    			var sum = document.forms[0].sum.value;
    			alert("Thank you for your donation of " + sum + " for " + projectname);
			}
		</script>
	</head>
	<body>
		<c:if test="${ not empty error }">
			Error: <c:out value="${sessionScope.error}" />
			<c:remove var="error" scope="session" />
	  	</c:if>
		<form onsubmit="confirmInput()" action="" method="get">
				<label for="variant">Enter sum to donate for <span class="projectname"><c:out value="${sessionScope.project.name}" /></span> project</label>
				<input id="variant" name="sum" size="20"/>
				<input type="submit" value="Go!">
	  	</form>
	</body>
</html>