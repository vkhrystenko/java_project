<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
	<head>
		<title>Kickstarter specific project</title>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/styles/project.css" />
	</head>
	<body bgcolor=white>
		<table>
		<tr>
			<th>Name: </th>
			<th><c:out value="${sessionScope.project.name}"/></th>
		</tr>
		<tr>
			<td>Description: </td>
			<td><c:out value="${sessionScope.project.description}"/></td>
		</tr>
		<tr>
			<td>TotalAmount/CollectAmount: </td>
			<td><c:out value="${sessionScope.project.donation.totalAmount}/${project.donation.collectAmount}"/></td>
		</tr>
		<tr>
			<td>DaysLeft: </td>
			<td><c:out value="${sessionScope.project.donation.daysLeft}"/></td>
		</tr>
		<tr>
			<td>Category: </td>
			<td><c:out value="${sessionScope.project.category.name}"/></td>
		</tr>
		<tr>
			<td>Link: </td>
			<td><c:out value="${sessionScope.project.link}"/></td>
		</tr>
		<tr>
			<td>Questions_answers: </td>
			<td><c:out value="${sessionScope.project.questionsAndAnswers}"/></td>
		</tr>
		</table>
		<form action="/kickstarter/donateproject" method="get">
				<input type="submit" value="Donate to this project">
	  	</form>
	  	<form action="/kickstarter/categories" method="get">
				<input type="submit" value="Home">
	  	</form>
	</body>

</html>
