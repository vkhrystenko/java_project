<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
	<head>
		<title>Kickstarter</title>
		<meta charset="utf-8" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/styles/categories.css" />
	</head>
	
	<body>
		<div>
			<h3><c:out value="${quote.getName()}"/></h3>
		
			<h2>Categories</h2>
			<ul id="menu" style="list-style-type:none">
			<c:forEach var="category" items="${categories}">
   				<li><c:out value="${category.id}"/> - <c:out value="${category.name}"/></li>
			</c:forEach>
			</ul>
		</div>
		<p>
		<c:if test="${ not empty error }">
			Error: <c:out value="${sessionScope.error}" />
			<c:remove var="error" scope="session" />
		</c:if>
		<form action="/kickstarter/projects" method="get">
			<label for="variant">Please select your variant (enter number of category)</label>
			<input id="variant" name="category" size="2"/>
			<input type="submit" value="Go!">
		</form>
	</body>

</html>

