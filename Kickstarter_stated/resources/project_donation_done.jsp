<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<html>
	<head>
		<title>thank you!!!</title>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/styles/donation_done.css" />
	</head>
	<body>
		Your donation has been submitted.<br>
		<a href="<c:url value="project">
		   <c:param name="projectid" value="${sessionScope.project.id}"/></c:url>">Go back to project</a><br>
		<a href="/kickstarter/categories">Go back to home page</a>
	</body>
</html>